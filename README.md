# bpm-parent

#### 介绍
bpm 

主要包含完整流程引擎功能，流程设计器、表单设计器、动态路由、我的待办/已办/草稿/申请 等。页面兼容移动端且满足各种中国式流程，力争成为开源流程引擎的标杆。

[在线体验地址](http://120.77.218.141:9992/bpm-web/index.html)

[前端项目路径](https://gitee.com/zhuzl002/bpm-web)
#### 开源目的

源于开源，回馈开源 。目前国内缺乏一个完整的开源流程引擎，因此想弥补一下这个空白，为后浪降低壁垒-。-
#### 软件架构

前端： vue + elementui
后端：

springboot 2.2.X

dubbo 2.6.5

mysql 5.8

redis

配置中心： nacos 1.2.0
注册中心： nacos 1.2.0
发布部署： jenkins 2.50.0
MAVEN私有仓库： nexus 3.X

#### 安装教程

1.  安装 nacos

2.  创建 nacos 对应配置文件（记得修改 nacos 及 数据库地址/用户名/密码）

    2.1）easy-bpm-parent\easy-bpm-web\src\main\resources\nacosConfig\easy-bpm-web.properties
    
    2.2）easy-bpm-parent\easy-bpm-provider\src\main\resources\nacosConfig\easy-bpm-provider.properties
    
3.  安装 mysql
4.  运行 easy-bpm-parent\easy-bpm-provider\src\main\resources\db\1.0.0\best_bpm.sql 表结构SQL

#### 使用说明

1.  如需新增表，则只需在 easy-parent\easy-bpm-provider\src\main\java\com.pig.easy.bpm\generator\MybaticPlusGenerator.java 修改一下表，点击一下运行
 即可生成 DO/DTO/VO/service/impl/controller 等常用CRUD方法，目前已经无缝与 swaggerUI 打通，自动添加文档。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 问题反馈及讨论
邮箱： 786289666@qq.com

QQ讨论群： 957664677

#### License
[AGPL3.0](https://www.gnu.org/licenses/agpl-3.0.txt) license.
Copyright (c) 2020-present pig

#### 打赏
<img src="https://gitee.com/zhuzl002/bpm-web/raw/master/src/assets/images/20200820181716.jpg" width="450" height="350" alt="note"/>
<img src="https://gitee.com/zhuzl002/bpm-web/raw/master/src/assets/images/20200820181724.jpg" width="450" height="350" alt="note"/>
