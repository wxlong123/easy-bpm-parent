package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.MenuQueryDTO;
import com.pig.easy.bpm.dto.request.MenuSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MenuDTO;
import com.pig.easy.bpm.dto.response.MenuTreeDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.MenuService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.MenuQueryVO;
import com.pig.easy.bpm.vo.request.MenuSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@RestController
@Api(tags = "菜单管理", value = "菜单管理")
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Reference
    MenuService service;

    @ApiOperation(value = "查询菜单列表", notes = "查询列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@Valid @RequestBody MenuQueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MenuQueryDTO queryDTO = switchToDTO(param, MenuQueryDTO.class);

        Result<PageInfo<MenuDTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "查询树形菜单", notes = "查询树形菜单", produces = "application/json")
    @PostMapping("/getMenuTree/{tenantId}")
    public JsonResult getList(@ApiParam(required = true, name = "租户编号", value = "tenantId", example = "pig") @PathVariable("tenantId") String tenantId) {

        BestBpmAsset.isAssetEmpty(tenantId);
        Result<List<MenuTreeDTO>> result = service.getMenuTree(tenantId, null);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增菜单", notes = "新增菜单", produces = "application/json")
    @PostMapping("/insert")
    public JsonResult insertMenu(@Valid @RequestBody MenuSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MenuSaveOrUpdateDTO saveDTO = switchToDTO(param, MenuSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insertMenu(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "修改菜单", notes = "修改菜单", produces = "application/json")
    @PostMapping("/update")
    public JsonResult updateMenu(@Valid @RequestBody MenuSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MenuSaveOrUpdateDTO saveDTO = switchToDTO(param, MenuSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.updateMenu(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除菜单", notes = "删除菜单", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@Valid @RequestBody MenuSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        MenuSaveOrUpdateDTO saveDTO = switchToDTO(param, MenuSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.deleteMenu(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

}

