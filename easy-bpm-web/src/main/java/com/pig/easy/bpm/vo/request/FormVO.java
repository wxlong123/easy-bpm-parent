package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/29 10:48
 */
@Data
@ToString
public class FormVO implements Serializable {

    private static final long serialVersionUID = -762300981964524645L;

    private Long formId;

    private String formKey;

    private String formName;

    private String tenantId;

    private Integer sort;

    private String formData;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private Integer pageSize;

    private Integer pageIndex;
}
