package com.pig.easy.bpm.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author pig
 * @since 2020-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("bpm_process_detail")
public class ProcessDetailDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程详细编号
     */
    @TableId(value = "process_detail_id", type = IdType.AUTO)
    private Long processDetailId;
    /**
     * 租户编号
     */
    @TableField("tenant_id")
    private String tenantId;
    @TableField("process_id")
    private Long processId;
    /**
     * 流程XML格式数据
     */
    @TableField("process_xml")
    private String processXml;

    /**
     * 申请标题规则
     */
    @TableField("apply_title_rule")
    private String applyTitleRule;
    /**
     * 流程到期时间
     */
    @TableField("apply_due_date")
    private Date applyDueDate;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 状态 1 有效 0 失效
     */
    @TableField("valid_state")
    private Integer validState;
    @TableField("operator_id")
    private Long operatorId;
    @TableField("operator_name")
    private String operatorName;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     *
     * flowable 流程版本号
     */
    @TableField("definition_id")
    private String definitionId;

    /**
     *
     * 1 未发布 2 已发布
     */
    @TableField("publish_status")
    private Integer publishStatus;

    /**
     *
     * 1 默认版本 2 非默认版本
     */
    @TableField("main_version")
    private Integer mainVersion;

    @TableField("auto_complete_first_node")
    private Integer autoCompleteFirstNode;
}
