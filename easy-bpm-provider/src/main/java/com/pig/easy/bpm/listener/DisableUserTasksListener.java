package com.pig.easy.bpm.listener;

import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.constant.TaskConstant;
import com.pig.easy.bpm.dto.request.UserTaskUpdateDTO;
import com.pig.easy.bpm.dto.response.UserTaskDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.UserTaskService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * todo: 取消当前节点的并行任务或者配置了完成条件的任务,清除完成 加签及指定人员
 * 是根据执行树去删除让
 *
 * @author : pig
 * @date : 2020/07/13 11:45
 */
@Component
@Slf4j
public class DisableUserTasksListener implements ExecutionListener {

    private static final long serialVersionUID = 7847355922619440599L;

    @org.apache.dubbo.config.annotation.Reference
    private UserTaskService userTaskService;

    @Autowired
    private TaskService taskService;

    @Override
    public void notify(DelegateExecution delegateExecution) {

        log.info("###############DisableUserTasksListener######[{}][{}]######start#####", delegateExecution.getCurrentActivityId(), delegateExecution.getCurrentFlowElement().getName());

        String approveAction = (String) delegateExecution.getVariable(TaskConstant.APPROVE_ACTION_DESC);
        Long applyId = CommonUtils.evalLong(delegateExecution.getVariable(BpmConstant.APPLY_ID));

        if (applyId > 0) {
            /* 获取执行树下的任务编号 */
            List<Task> list = taskService.createTaskQuery().processInstanceId(delegateExecution.getProcessInstanceId()).taskDefinitionKey(delegateExecution.getCurrentActivityId()).list();

            HashMap<String, Task> flowUserTaskMap = new HashMap<>();
            for (Task task : list) {
                flowUserTaskMap.put(task.getId(), task);
            }

            Result<List<UserTaskDTO>> result3 = userTaskService.getUserTaskByApplyId(applyId);
            if (result3.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return;
            }
            result3.getData().stream().filter(temp ->
                    temp.getTaskStatus() < TaskConstant.TASK_COMPLETED
                            && flowUserTaskMap.get(temp.getActTaskId()) == null).forEach(
                    userTask -> {
                        UserTaskUpdateDTO disableUserTask = BeanUtils.switchToDTO(userTask, UserTaskUpdateDTO.class);
                        disableUserTask.setUpdateTime(LocalDateTime.now());
                        disableUserTask.setTaskStatus(TaskConstant.TASK_DISABLE);
                        disableUserTask.setApproveTime(LocalDateTime.now());
                        userTaskService.updateUserTask(disableUserTask);
                    }
            );
        }

        log.info("###############DisableUserTasksListener######[{}][{}]######end#####", delegateExecution.getCurrentActivityId(), delegateExecution.getCurrentFlowElement().getName());
    }
}
