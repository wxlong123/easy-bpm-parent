package com.pig.easy.bpm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.FileTempleteQueryDTO;
import com.pig.easy.bpm.dto.request.FileTempleteSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.FileTempleteDTO;
import com.pig.easy.bpm.entity.FileTempleteDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.FileTempleteMapper;
import com.pig.easy.bpm.service.FileTempleteService;
import com.pig.easy.bpm.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 模板文件表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-08-10
 */
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class FileTempleteServiceImpl extends BeseServiceImpl<FileTempleteMapper, FileTempleteDO> implements FileTempleteService {

    @Autowired
    FileTempleteMapper mapper;

    @Override
    public Result<PageInfo<FileTempleteDTO>> getListByCondition(FileTempleteQueryDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        int pageIndex = CommonUtils.evalInt(param.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(param.getPageSize(), DEFAULT_PAGE_SIZE);

        PageHelper.startPage(pageIndex, pageSize);
        param.setValidState(VALID_STATE);
        List<FileTempleteDTO> list = mapper.getListByCondition(param);
        if (list == null) {
            list = new ArrayList<>();
        }
        PageInfo<FileTempleteDTO> pageInfo = new PageInfo<>(list);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<List<FileTempleteDTO>> getListByProcessIdAndTenantId(String tenantId, Long processId) {

        BestBpmAsset.isEmpty(tenantId);
        BestBpmAsset.isEmpty(processId);

        List<FileTempleteDO> selectList = mapper.selectList(new QueryWrapper(FileTempleteDO.builder().tenantId(tenantId).processId(processId).validState(VALID_STATE).build()));
        if(selectList == null){
            selectList = new ArrayList<>();
        }

        List<FileTempleteDTO> result = new ArrayList<>();
        FileTempleteDTO fileTempleteDTO = null;
        for (FileTempleteDO fileTempleteDO : selectList) {
             fileTempleteDTO = BeanUtils.switchToDTO(fileTempleteDO, FileTempleteDTO.class);
            result.add(fileTempleteDTO);
        }
        return Result.responseSuccess(result);
    }


    @Override
    public Result<Integer> insertFileTemplete(FileTempleteSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        if (StringUtils.isEmpty(param.getTempalteId())) {
            param.setTempalteId(new RandomGUID().toString());
        }


        FileTempleteDO temp = BeanUtils.switchToDO(param, FileTempleteDO.class);
        Integer num = mapper.insert(temp);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> updateFileTemplete(FileTempleteSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        FileTempleteDO temp = BeanUtils.switchToDO(param, FileTempleteDO.class);
        Integer num = mapper.updateById(temp);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> deleteFileTemplete(FileTempleteSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        FileTempleteDO temp = BeanUtils.switchToDO(param, FileTempleteDO.class);
        temp.setValidState(INVALID_STATE);
        Integer num = mapper.updateById(temp);
        return Result.responseSuccess(num);
    }

}
