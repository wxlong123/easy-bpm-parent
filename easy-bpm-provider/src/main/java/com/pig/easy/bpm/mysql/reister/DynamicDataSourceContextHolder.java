package com.pig.easy.bpm.mysql.reister;

import java.util.ArrayList;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/9 10:32
 */
public class DynamicDataSourceContextHolder {

    private static final ThreadLocal<String> contextHolder = new ThreadLocal<>();
    public  static List<String> dataSourceIds = new ArrayList<>();

    public static void setDataSourceType(String dataSourceType) {
        if(!containsDataSource(dataSourceType)){
            contextHolder.set(dataSourceType);
        }
    }

    public static String getDataSourceType() {
        return contextHolder.get();
    }

    public static void clearDataSourceType() {
        contextHolder.remove();
    }

    /**
     * 判断指定DataSrouce当前是否存在
     *
     * @param dataSourceId
     * @return
     */
    public static boolean containsDataSource(String dataSourceId){
        return dataSourceIds.contains(dataSourceId);
    }
}
