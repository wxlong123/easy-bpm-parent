package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.RoleGroupDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 角色组 服务类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
public interface RoleGroupService {

    Result<RoleGroupDTO> getRoleGroupByCode(String roleGroupCode);

    Result<RoleGroupDTO> getRoleGroupById(Long roleGroupId);

    Result<List<ItemDTO>> getRoleGroupDict(String tenantId);
}
