package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 17:33
 */
@Data
@ToString
public class DictQueryDTO extends BaseRequestDTO {

    private static final long serialVersionUID = 3306028290149586790L;

    private Long dictId;

    private String dictCode;

    private String dictName;

    private String tenantId;

    private String remark;

    private Integer validState;

    private int pageIndex;

    private int pageSize;
}
