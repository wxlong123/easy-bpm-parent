package com.pig.easy.bpm.dubbo.config;/**
 * Created by Administrator on 2020/4/14.
 */

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.apache.dubbo.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/4/14 14:42
 */
public class DubboConfigConfiguration {

    private Logger logger = LoggerFactory.getLogger(DubboConfigConfiguration.class);

    private static final String PROTOCOL_NAME = "dubbo";
    private static final String PROTOCOL_HOST = "localhost";
    private static final String DEFAULT_FILTER = "globalTraceFilter";
    private static final Integer RETRIES_NUM = 0;
    private static final boolean ENABLE_GLOBAL_TRACE_TILTER = Boolean.TRUE;

    @NacosValue(value = "${bpm.dubbo.server.name:}", autoRefreshed = true)
    private String serverName;

    @NacosValue(value = "${bpm.dubbo.registry.address:}", autoRefreshed = true)
    private String registryAddress;

    @NacosValue(value = "${bpm.dubbo.server.port:}", autoRefreshed = true)
    private String port;

    @NacosValue(value = "${bpm.dubbo.server.timeout:}", autoRefreshed = true)
    private String timeout;

    @Bean
    public ApplicationConfig applicationConfig() {
        this.checkData();
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName(serverName);
        applicationConfig.setQosEnable(false);
        applicationConfig.setQosAcceptForeignIp(true);
        applicationConfig.setQosPort(Integer.valueOf(port));
        applicationConfig.setId(serverName);

        logger.info("applicationConfig {}",applicationConfig);
        return applicationConfig;
    }

    @Bean
    public ProtocolConfig protocolConfig() {
        this.checkData();
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setPort(Integer.valueOf(port));
        protocolConfig.setName(PROTOCOL_NAME);
        protocolConfig.setHost(PROTOCOL_HOST);
        return protocolConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        this.checkData();

        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(registryAddress);
        registryConfig.setRegister(true);
        logger.info("registryAddress {}",registryAddress);
        return registryConfig;
    }

    @Bean
    public ProviderConfig providerConfig() {
        this.checkData();
        ProviderConfig providerConfig = new ProviderConfig();
        providerConfig.setTimeout(NumberUtils.parseNumber(timeout,Integer.class));
        providerConfig.setRetries(RETRIES_NUM);
        if(ENABLE_GLOBAL_TRACE_TILTER){
            providerConfig.setFilter(DEFAULT_FILTER);
        }
        return providerConfig;
    }

    @Bean
    public ConsumerConfig consumerConfig() {
        this.checkData();
        ConsumerConfig consumerConfig = new ConsumerConfig();
        consumerConfig.setTimeout(NumberUtils.parseNumber(timeout,Integer.class));
        consumerConfig.setRetries(RETRIES_NUM);
        consumerConfig.setCheck(false);
        if(ENABLE_GLOBAL_TRACE_TILTER){
            consumerConfig.setFilter(DEFAULT_FILTER);
        }
        return consumerConfig;
    }

    private void checkData() {
        if (StringUtils.isEmpty(serverName)
                || StringUtils.isEmpty(port)
                || StringUtils.isEmpty(registryAddress)
                || NumberUtils.parseNumber(timeout,Integer.class) < 0) {
            throw new RuntimeException("dubboConfiguration init fail, please config on nacos");
        }
        logger.info("current dubbo config, serverName {}, port {},registryAddress {},timeout {}",serverName,port,registryAddress,timeout);
    }

    private RegistryConfig createRegistryConfig(String registryAddress, Boolean register) {
        this.checkData();
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(registryAddress);
        registryConfig.setRegister(register);
        return registryConfig;
    }

}
